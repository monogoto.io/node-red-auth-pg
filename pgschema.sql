--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5 (Debian 14.5-1.pgdg110+1)
-- Dumped by pg_dump version 14.5

-- Started on 2022-12-05 20:21:36

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 74856)
-- Name: data; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA data;


ALTER SCHEMA data OWNER TO postgres;

--
-- TOC entry 2 (class 3079 OID 74857)
-- Name: citext; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;


--
-- TOC entry 3418 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION citext; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION citext IS 'data type for case-insensitive character strings';


--
-- TOC entry 3 (class 3079 OID 74968)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- TOC entry 3419 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 212 (class 1259 OID 74979)
-- Name: users; Type: TABLE; Schema: data; Owner: postgres
--

CREATE TABLE data.users (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    name public.citext NOT NULL,
    pass_md5 public.citext NOT NULL,
    permissions text DEFAULT '*'::text NOT NULL,
    services text[],
    last_login_success timestamp without time zone
);


ALTER TABLE data.users OWNER TO postgres;

--
-- TOC entry 3420 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN users.services; Type: COMMENT; Schema: data; Owner: postgres
--

COMMENT ON COLUMN data.users.services IS 'null means all services.
empty array means none.';


--
-- TOC entry 3273 (class 2606 OID 74989)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 3271 (class 1259 OID 74987)
-- Name: users_idx_name; Type: INDEX; Schema: data; Owner: postgres
--

CREATE UNIQUE INDEX users_idx_name ON data.users USING btree (name);


-- Completed on 2022-12-05 20:21:36

--
-- PostgreSQL database dump complete
--

