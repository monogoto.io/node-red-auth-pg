const crypto = require('crypto');
const postgres = require('postgres');

module.exports = function dbauth(connectionString, serviceName, logger) {
    
    const noderedAuthDbConn = postgres(connectionString)
    noderedAuthDbConn`SELECT NOW()`.catch(e => {
      throw new Error('Failed to init postgres auth db connection', e);
    });
    
    async function selectUserWithPerms(username) {
        const rows = await noderedAuthDbConn`SELECT * FROM data.users WHERE name = ${username}`;
        if (rows?.length) {
            const user = { username: rows[0].name, permissions: rows[0].permissions };
            return user;
        }
        return null;
    }

    return {
        type: "credentials",
        users: async function(username) {
          try {
            return await selectUserWithPerms(username);
          } catch(e) {
            logger.error('NODERED_AUTHDB "users" query failed', e);
          }  
          return null;
        },
        authenticate: async function(username, password) {
          try {
            const passHash = crypto.createHash('md5').update(password).digest('hex')
            const user = (await noderedAuthDbConn`SELECT * FROM data.users WHERE name = ${username} AND pass_md5 = ${passHash}`)?.[0];

            if(!user) return null;

            if(user.services === null || (Array.isArray(user.services) && user.services.includes(serviceName))) {
              // null means all servies allowed, array means only specific ones.
              await noderedAuthDbConn`UPDATE data.users SET last_login_success = now() WHERE id = ${user.id}`;
              return {username: user.name, permissions: user.permissions};
            }
          } catch(e) {
            logger.error('NODERED_AUTHDB "authenticate" query failed', e);
          }  
          return null
        },
        default: async function() {
            return {anonymous: true, permissions:null}; // permissions=null means no access to anonymous users, must login
        }
     }
}